from django.contrib import admin
from django.utils.html import format_html
from products.models import Category, Contact, Gallery, Group, OurWork, Preference, Product, Service

# Register your models here.

class ServiceAdmin(admin.ModelAdmin):
    list_display = ['image_tag', 'id', 'created_at']
    list_display_links = ['id']
    search_fields = ('id',)


    def image_tag(self, obj):
        if obj.image:
            url = obj.image.url
        else:
            url = ''
        return format_html('<img src="{}" style="max-height: 60px; width: 100px; object-fit: contain;" />'.format(url))

    image_tag.short_description = 'Image'


class OurWorkAdmin(admin.ModelAdmin):
    list_display = ['image_tag', 'title', 'payback']
    list_display_links = ['title']
    search_fields = ('title',)


    def image_tag(self, obj):
        if obj.image:
            url = obj.image.url
        else:
            url = ''
        return format_html('<img src="{}" style="max-height: 60px; width: 100px; object-fit: contain;" />'.format(url))

    image_tag.short_description = 'Image'


class ProductAdmin(admin.ModelAdmin):
    list_display = ['image_tag', 'name', 'created_at']
    list_display_links = ['name']
    search_fields = ('name',)


    def image_tag(self, obj):
        if obj.image_1:
            url = obj.image_1.url
        else:
            url = ''
        return format_html('<img src="{}" style="max-height: 60px; width: 100px; object-fit: contain;" />'.format(url))

    image_tag.short_description = 'Image'


class ContactAdmin(admin.ModelAdmin):
    list_display = ['phone', 'fullname', 'created_at']
    list_display_links = ['phone']
    search_fields = ('fullname', 'phone')




class PreferenceAdmin(admin.ModelAdmin):
    search_fields = ('key', 'group__name', 'display_name')
    list_display = ('id', 'display_name', 'value', 'key')
    # ordering = ('order',)


class PreferenceInlineAdmin(admin.TabularInline):
    model = Preference
    fields = ('display_name', 'key', 'value', 'type', 'file')
    # exclude = ('key', 'order')
    # readonly_fields = ('display_name', 'type')

    # ordering = ('order',)

    class Media:
        js = ('js/admin/preferences.js',)

    def get_readonly_fields(self, request, obj=None):
        if obj and not request.user.is_superuser:
            return self.readonly_fields + ('key',)
        return self.readonly_fields

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class GroupAdmin(admin.ModelAdmin):
    inlines = (PreferenceInlineAdmin,)
    list_display = ('name',)







admin.site.register(Group, GroupAdmin)
admin.site.register(Preference, PreferenceAdmin)


admin.site.register(Category)
admin.site.register(Contact, ContactAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Gallery)
admin.site.register(Service, ServiceAdmin)
admin.site.register(OurWork, OurWorkAdmin)
