from rest_framework import serializers

from products.models import Category, Contact, Gallery, OurWork, Preference, Product, Service


class PreferenceSerializer(serializers.ModelSerializer):
    value = serializers.SerializerMethodField()
    group_name = serializers.SerializerMethodField()
    class Meta:
        model = Preference
        fields = ['key', 'display_name', 'type', 'value', 'group_name']

    def get_value(self, obj):
        if obj.type == 'file':
            return self.context.get('request').build_absolute_uri(obj.file.url)
        return obj.value

    def get_group_name(self, obj):
        if obj.group:
            return obj.group.name
        return None


class GalleryListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Gallery
        fields = ['id', 'image']


class CategoryListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = ['id', 'name']


class ProductListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = ['id', 'name', 'power', 'image_1']


class ProductDetailSerializer(ProductListSerializer):

    class Meta:
        model = Product
        fields = "__all__"



class ServiceListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Service
        fields = ['id', 'desc', 'image', 'icon']


class ContactCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Contact
        fields = ['fullname', 'phone', 'message']


class OurWorkListSerializer(serializers.ModelSerializer):

    class Meta:
        model = OurWork
        fields = "__all__"