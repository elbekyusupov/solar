from django.shortcuts import render
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import mixins, viewsets, filters

from products.models import Category, Contact, Gallery, OurWork, Preference, Product, Service
from products.serializers import CategoryListSerializer, ContactCreateSerializer, GalleryListSerializer, OurWorkListSerializer, PreferenceSerializer, ProductDetailSerializer, ProductListSerializer, ServiceListSerializer

# Create your views here.


class PreferenceViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    serializer_class = PreferenceSerializer
    ordering_fields = ['name']
    filter_backends = [DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter]
    filterset_fields = ['key', 'group__name']

    def get_queryset(self):
        return Preference.objects.all()


class ProductViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    serializer_class = ProductListSerializer
    ordering_fileds = ['created_at']
    filter_backends = [DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter]
    filterset_fields = ['name', 'category__id']
    search_fields = ['id']

    def get_queryset(self):
        return Product.objects.all()

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return ProductDetailSerializer
        return ProductListSerializer

class CategoryViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    serializer_class = CategoryListSerializer
    ordering_fileds = ['created_at']
    filter_backends = [DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter]
    filterset_fields = ['name']
    search_fields = ['id']

    def get_queryset(self):
        return Category.objects.all()


class GalleryViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    serializer_class = GalleryListSerializer
    ordering_fileds = ['created_at']
    filter_backends = [DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter]
    filterset_fields = ['id']
    search_fields = ['id']

    def get_queryset(self):
        return Gallery.objects.all()

class ServiceViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    serializer_class = ServiceListSerializer
    ordering_fileds = ['created_at']
    filter_backends = [DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter]
    filterset_fields = ['id']
    search_fields = ['id']

    def get_queryset(self):
        return Service.objects.all()

class OurWorkViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    serializer_class = OurWorkListSerializer
    ordering_fileds = ['created_at']
    filter_backends = [DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter]
    filterset_fields = ['id']
    search_fields = ['id']

    def get_queryset(self):
        return OurWork.objects.all()

class ContactViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    serializer_class = ContactCreateSerializer


    def get_queryset(self):
        return Contact.objects.all()
