from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField

# Create your models here.


class Category(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return '%d: %s' % (self.id, self.name)

class Product(models.Model):
    name = models.CharField(max_length=255)
    desc = RichTextUploadingField()
    image_1 = models.FileField(upload_to='Products', null=True, blank=True)
    image_2 = models.FileField(upload_to='Products', null=True, blank=True)
    image_3 = models.FileField(upload_to='Products', null=True, blank=True)
    price = models.FloatField(null=True, blank=True)
    power = models.CharField(max_length=100, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    category = models.ForeignKey(
        Category,
        related_name='products',
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name



class Gallery(models.Model):
    image = models.FileField(upload_to='Gallery')

    def __str__(self):
        return str(self.id)



class Service(models.Model):
    desc = RichTextUploadingField()
    image = models.FileField(upload_to='Products', null=True, blank=True)
    icon = models.FileField(upload_to='Products', null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.id)



class OurWork(models.Model):
    title = models.CharField(max_length=255)
    image = models.FileField(upload_to='Products', null=True, blank=True)
    address = models.CharField(max_length=255)
    payback = models.CharField(max_length=255)
    power = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.title)


class Contact(models.Model):
    fullname = models.CharField(max_length=255)
    phone = models.CharField(max_length=20)
    message = models.CharField(max_length=500)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return str(self.phone)


class Group(models.Model):
    name = models.CharField(max_length=255)
    description = models.CharField(max_length=1000, blank=True, null=True)

    def __str__(self):
        return self.name


class Preference(models.Model):
    TYPE_CHOICES = (
        ('char', 'CharField'),
        ('float', 'FloatField'),
        ('boolean', 'BooleanField'),
        ('integer', 'IntegerField'),
        ('file', 'FileField'),
    )

    key = models.CharField(max_length=255)
    display_name = models.CharField(max_length=255, null=True, blank=True)
    type = models.CharField(choices=TYPE_CHOICES, default='char', max_length=50)
    value = models.CharField(max_length=2000, blank=True, null=True)
    file = models.FileField(upload_to='settings', blank=True, null=True)
    group = models.ForeignKey(
        Group,
        on_delete=models.CASCADE,
        related_name="preferences"
    )

    def __str__(self):
        return self.key
