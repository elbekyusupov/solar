from rest_framework.routers import DefaultRouter

from products.views import CategoryViewSet, ContactViewSet, GalleryViewSet, OurWorkViewSet, PreferenceViewSet, ProductViewSet, ServiceViewSet


router = DefaultRouter()

router.register('products', ProductViewSet, basename='products')
router.register('categories', CategoryViewSet, basename='categories')
router.register('preferences', PreferenceViewSet, basename='preferences')
router.register('gallery', GalleryViewSet, basename='gallery')
router.register('services', ServiceViewSet, basename='services')
router.register('our-works', OurWorkViewSet, basename='our-works')
router.register('contact', ContactViewSet, basename='contact')

urlpatterns = router.urls
